## Emojis Explained
- ❓: Not sure, unknown, not important.
- 🟩: Mute or Block is currently active.
- 🟥: Mute or Block is currently not active.
- 🟧: Mute or Block is not proccessed yet.

## Instance Mutes

| Active | Instance   | Date     | Reason                                             |
|:------:|------------|----------|----------------------------------------------------|
|   ❓   | None       |          |                                                    |

## Instance Blocks

| Active | Instance   | Date     | Reason                                             |
|:------:|------------|----------|----------------------------------------------------|
|   🟩   | yandere.cc | 06-03-23 | Hentain bot spam, sharing illegal content (minors) |